provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_dynamodb_table" "db_credstash" {
  name = "credential-store"
  hash_key = "empid"
  attribute {
    name = "empid"
    type = "S"
  }
  attribute {
    name = "name"
    type = "S"
  }
  attribute {
    name = "salary"
    type = "S"
  }
}
# Creating s3 resource for invoking to lambda function
resource "aws_s3_bucket" "bucket" {
bucket = "my-test-bucket-eu-west-1"
acl    = "private"
tags = {
Name        = "My bucket"
Environment = "Dev"
}
}
resource "aws_lambda_function" "terraform_function" {
  filename         = "upload_file_in_dynmo_db.zip"
  function_name    = "terraform_function"
  handler          = "lambda.handler"
  role             = "${aws_iam_role.iam_for_lambda.arn}"
  runtime          = "python 3.6"
  source_code_hash = "${filebase64sha256("upload_file_in_dynmo_db.zip")}"
}
# Adding S3 bucket as trigger to my lambda and giving the permissions
resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
bucket = "${aws_s3_bucket.bucket.id}"
lambda_function {
lambda_function_arn = "${aws_lambda_function.terraform_function.arn}"
events              = ["s3:ObjectCreated:*"]
filter_suffix       = ".csv"
}
}
# output of lambda arn
output "arn" {
value = "${aws_lambda_function.test_lambda.arn}"
}
# creating lambda to fetch data from databases
resource "aws_lambda_function" "FetchDatafromDb" {

  function_name = "FetchDatafromDb"
  filename = "api_lambda.zip"
  handler = "api_lambda.py.lambda_handler"
  runtime = "python3.8"
  source_code_hash = filebase64sha256("api_lambda.py")
  role = ${aws_iam_role.iam_for_lambda.arn})

  environment {
      Emp_TABLE = aws_dynamodb_table.db_credstash.name
   }

}
resource "aws_api_gateway_rest_api" "emp_apigw" {
  name        = "emp_apigw"
  description = "Emp API Gateway"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}
resource "aws_api_gateway_resource" "employee" {
  rest_api_id = aws_api_gateway_rest_api.emp_apigw.id
  parent_id   = aws_api_gateway_rest_api.emp_apigw.root_resource_id
  path_part   = "employee"
}
resource "aws_api_gateway_method" "empupdate" {
  rest_api_id   = aws_api_gateway_rest_api.emp_apigw.id
  resource_id   = aws_api_gateway_resource.employee.id
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "lambda_intergration" {

  rest_api_id = aws_api_gateway_rest_api.emp_apigw.id
  resource_id = aws_api_gateway_method.empupdate.resource_id
  http_method = aws_api_gateway_method.empupdate.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"

  uri = aws_lambda_function.FetchDatafromDb.invoke_arn
}

resource "aws_lambda_permission" "empHandeler" {

  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.FetchDatafromDb.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.emp_apigw.execution_arn}/*/POST/employee"
}
resource "aws_api_gateway_deployment" "empapideployment" {

  depends_on = [
    aws_api_gateway_integration.lambda_intergration
  ]

  rest_api_id = aws_api_gateway_rest_api.emp_apigw.id
  stage_name  = "Dev"
}

